package com.chucklingcheese.csvtool.lang;

/**
 * 
 * Information relevant to different postage costs in respect to different zoning specifications, as well as other
 * oddities and exceptions.
 * 
 * This is now mostly redundant since switching to the Royal Mail, however we can still use these to keep an eye out
 * for post codes we aren't expecting to see.
 * 
 * Sources for this information can be found at the links below.
 * 
 * Parcelforce pricing:
 * https://www.parcelforce.com/sites/default/files/UKPostcodelistv2Aug19.pdf
 * https://www.parcelforce.com/business-parcels/uk-parcel-delivery/understanding-prices
 * 
 * General information:
 * https://en.wikipedia.org/wiki/List_of_postcode_areas_in_the_United_Kingdom
 * https://en.wikipedia.org/wiki/Postcodes_in_the_United_Kingdom
 * 
 * Values are correct and accurate as of Saturday 02 May 2020.
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 2.0.0
 * 
 */
public class PostageInfo {
	
	/**
	 * Area codes within the Channel Islands.
	 * Considered as international by Parcelforce; their postage varies.
	 */
	public static final String[] CHANNEL_ISLANDS = {
			"GY", "JE"
	};
	
	/**
	 * Deprecated area codes which have been replaced.
	 * These cannot be used by any courier as they are invalid.
	 */
	public static final String[] DEPRECATED = {
			"CRO", "NOR"
	};
	
	/**
	 * Area codes within the Isle of Man.
	 * Considered as "zone three" by Parcelforce; their postage is £17.95.
	 */
	public static final String[] ISLE_OF_MAN = {
			"IM"
	};
	
	/**
	 * Outer post codes within the Isle of Wight.
	 * Surcharged £5 by Parcelforce; their postage is £9.95.
	 */
	public static final String[] ISLE_OF_WIGHT = {
			"PO30", "PO31", "PO32", "PO33", "PO34", "PO35", "PO36", "PO37", "PO38", "PO39",
			"PO40", "PO41"
	};
	
	/**
	 * Outer post codes within the Isles of Scilly.
	 * Considered as "zone three" by Parcelforce; their postage is £17.95.
	 */
	public static final String[] ISLES_OF_SCILLY = {
			"TR21", "TR22", "TR23", "TR24", "TR25"
	};
	
	/**
	 * Area codes within Northern Ireland.
	 * Considered as "zone three" by Parcelforce; their postage is £17.95.
	 */
	public static final String[] NORTHERN_IRELAND = {
			"BT"
	};
	
	/**
	 * Area codes for overseas territories.
	 * Those using an incompatible post code structure are not included.
	 * Considered as international by Parcelforce; their postage varies.
	 */
	public static final String[] OVERSEAS_TERRITORIES = {
			// Ascension Island.
			"ASCN",
			// Indian Ocean Territory.
			"BBND",
			// Antarctic Territory.
			"BIQQ",
			// Falkland Islands.
			"FIQQ",
			// Gibraltar.
			"GX11",
			// Pitcairn Islands.
			"PCRN",
			// South Georgia & South Sandwich Islands.
			"SIQQ",
			// Saint Helena.
			"STHL",
			// Tristan de Cunha.
			"TDCU",
			// Turks & Caicos Islands.
			"TKCA"
	};
	
	/**
	 * Area codes used for non-geographic locations.
	 * These should reviewed manually for various reasons.
	 */
	public static final String[] NON_GEOGRAPHIC = {
			"GIR", "BF", "BFPO", "BX", "XX"
	};
	
	/**
	 * Outer post codes within the Scottish highlands and islands.
	 * Considered as "zone three" by Parcelforce; their postage is £12.95.
	 */
	public static final String[] SCOTTISH_HIGHLANDS_AND_ISLANDS = {
			// Aberdeen.
			"AB31", "AB33", "AB34", "AB35", "AB36", "AB37", "AB38", "AB45", "AB52", "AB53",
			"AB54", "AB55", "AB56",
			// Falkirk & Stirling.
			"FK17", "FK18", "FK19", "FK20", "FK21",
			// Outer Hebrides.
			"HS1",  "HS2",  "HS3",  "HS4",  "HS5",  "HS6",  "HS7",  "HS8",  "HS9",
			// Inverness.
			"IV1",  "IV2",  "IV3",  "IV10", "IV11", "IV12", "IV13", "IV14", "IV15", "IV16",
			"IV17", "IV18", "IV19", "IV20", "IV21", "IV22", "IV23", "IV24", "IV25", "IV26",
			"IV27", "IV28", "IV30", "IV31", "IV32", "IV36", "IV40", "IV41", "IV42", "IV43",
			"IV44", "IV45", "IV46", "IV47", "IV48", "IV49", "IV51", "IV52", "IV53", "IV54",
			"IV55", "IV56", "IV99",
			// Kilmarnock.
			"KA27", "KA28",
			// Kirkwall.
			"KW1",  "KW2",  "KW3",  "KW5",	"KW6",  "KW7",  "KW8",  "KW9", "KW10", "KW11",
			"KW12", "KW13", "KW14", "KW15", "KW16", "KW17",
			// Paisley.
			"PA20", "PA21", "PA22", "PA23", "PA24", "PA25", "PA26", "PA27", "PA28", "PA29",
			"PA30", "PA31", "PA32", "PA33", "PA34", "PA35", "PA36", "PA37", "PA38", "PA41",
			"PA42", "PA43", "PA44", "PA45", "PA46", "PA47", "PA48", "PA49", "PA60", "PA61",
			"PA62", "PA63", "PA64", "PA65", "PA66", "PA67", "PA68", "PA69", "PA70", "PA71",
			"PA72", "PA73", "PA74", "PA75", "PA76", "PA77", "PA80",
			// Perth.
			"PH17", "PH18", "PH19", "PH20", "PH21", "PH22", "PH23", "PH24", "PH25", "PH26",
			"PH30", "PH31", "PH32", "PH33", "PH34", "PH35", "PH36", "PH37", "PJ38", "PH39",
			"PH40", "PH41", "PH42", "PH43", "PH44", "PH49", "PH50",
			// Lerwick.
			"ZE1", "ZE2", "ZE3"
	};
	
}