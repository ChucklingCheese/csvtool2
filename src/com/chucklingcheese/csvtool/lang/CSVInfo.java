package com.chucklingcheese.csvtool.lang;

/**
 * 
 * Information relevant to the reading, writing, cleaning, and conversion of CSV order exports, as well as hopefully in
 * the future the matching up of reference numbers to tracking numbers.
 * 
 * Any changes made to the CSV header array values here **MUST** be reflected in the cleaner/converter tools!!!
 * 
 * Values are correct and accurate as of Thursday 27 August 2020.
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 2.0.0
 * 
 */
public class CSVInfo {
	
	/**
	 * Counter for BoroughBox CSV files.
	 */
	private static int countBoroughBox = 0;
	
	/**
	 * Counter for eBay CSV files.
	 */
	private static int countEbay = 0;
	
	/**
	 * Counter for Etsy CSV files.
	 */
	private static int countEtsy = 0;
	
	/**
	 * Counter for OnBuy CSV files.
	 */
	private static int countOnBuy = 0;
	
	/**
	 * Counter for TheFoodMarket CSV files.
	 */
	private static int countTheFoodMarket = 0;
	
	/**
	 * Counter for uDropship CSV files.
	 */
	private static int countuDropship = 0;
	
	/**
	 * Counter for Wowcher CSV files.
	 */
	private static int countWowcher = 0;
	
	/**
	 * Increments the counter for BoroughBox CSV files.
	 * @return Counter for BoroughBox CSV files.
	 */
	public static int incCountBoroughBox() {
		return ++countBoroughBox;
	}
	
	/**
	 * Increments the counter for eBay CSV files.
	 * @return Counter for eBay CSV files.
	 */
	public static int incCountEbay() {
		return ++countEbay;
	}
	
	/**
	 * Increments the counter for Etsy CSV files.
	 * @return Counter for Etsy CSV files.
	 */
	public static int incCountEtsy() {
		return ++countEtsy;
	}
	
	/**
	 * Increments the counter for OnBuy CSV files.
	 * @return Counter for OnBuy CSV files.
	 */
	public static int incCountOnBuy() {
		return ++countOnBuy;
	}
	
	/**
	 * Increments the counter for TheFoodMarket CSV files.
	 * @return Counter for TheFoodMarket CSV files.
	 */
	public static int incCountTheFoodMarket() {
		return ++countTheFoodMarket;
	}
	
	/**
	 * Increments the counter for uDropship CSV files.
	 * @return Counter for uDropship CSV files.
	 */
	public static int incCountuDropship() {
		return ++countuDropship;
	}
	
	/**
	 * Increments the counter for Wowcher CSV files.
	 * @return Counter for Wowcher CSV files.
	 */
	public static int incCountWowcher() {
		return ++countWowcher;
	}
	
	/**
	 * CSV headers required from BoroughBox.
	 */
	public static final String[] CSV_HEADERS_BB = {
			"Address 1",
			"Address 2",
			"City",
			"Country",
			"Firstname",
			"Lastname",
			"Order Date",
			"Order Number",
			"Phone",
			"Price",
			"Product Name",
			"Shipping Amount",
			"State",
			"SKU",
			"Qty",
			"Zip Code"
	};
	
	/**
	 * CSV headers required from eBay.
	 */
	public static final String[] CSV_HEADERS_EBAY = {
			"Buyer name",
			"Custom label",
			"Item number",
			"Item title",
			"Order number",
			"Post to address 1",
			"Post to address 2",
			"Post to city",
			"Post to county",
			"Post to country",
			"Post by date",
			"Post to name",
			"Post to phone",
			"Post to postcode",
			"Postage and packaging",
			"Quantity",
			"Sale date",
			"Sold for",
			"Buyer email"
	};
	
	/**
	 * CSV headers required from Etsy.
	 */
	public static final String[] CSV_HEADERS_ETSY = {
			"Buyer",
			"Currency",
			"Date Posted",
			"Delivery Address1",
			"Delivery Address2",
			"Delivery City",
			"Delivery Country",
			"Delivery Name",
			"Delivery State",
			"Delivery Zipcode",
			"Item Name",
			"Listing ID",
			"Order Delivery",
			"Order ID",
			"Price",
			"Quantity",
			"Sale Date",
			"SKU"
	};
	
	/**
	 * CSV headers required from OnBuy.
	 */
	public static final String[] CSV_HEADERS_OB = {
			"Customer",
			"Delivery Address Country",
			"Delivery Address County",
			"Delivery Address Line 1",
			"Delivery Address Line 2",
			"Delivery Address Line 3",
			"Delivery Address Name",
			"Delivery Address Postcode",
			"Delivery Address Town",
			"Delivery Service",
			"Order Date",
			"Order Number",
			"OPC",
			"Product Name",
			"Product Unit Price",
			"Quantity",
			"SKU",
			"Total Delivery"
	};
	
	/**
	 * CSV headers required from TheFoodMarket.
	 * Do not rearrange these values unless required!
	 */
	public static final String[] CSV_HEADERS_TFM = {
			"Order date",
			"Order Total",
			"Order Promotions",
			"Store Order Id",
			"Store Order Item Id",
			"SKU",
			"Item Name",
			"Quantity",
			"Amount",
			"Product Value",
			"Delivery Cost",
			"Total Value",
			"Currency Code",
			"Delivery Name",
			"Delivery Address 1",
			"Delivery Address 2",
			"Delivery Address 3",
			"Delivery Town City",
			"Delivery State County",
			"Post code/Zip",
			"Delivery Country",
			"Customer Phone Number",
			"Shipping Status",
			"Shipping Type",
			"Delivery Due Date",
			"Dispatched",
			"Shipping Cost Transactions",
			"Delivery Note",
			"Gift Message",
			"Shipment State",
			"Email"
	};
	
	/**
	 * CSV headers required from uDropship.
	 */
	public static final String[] CSV_HEADERS_UDS = {
			"bill_company",
			"bill_firstname",
			"bill_lastname",
			"created_at",
			"message",
			"order_id",
			"price",
			"product",
			"quantity",
			"ship_address_1",
			"ship_city",
			"ship_company",
			"ship_country",
			"ship_firstname",
			"ship_lastname",
			"ship_postcode",
			"ship_region",
			"ship_telephone",
			"shipping_amount",
			"sku"
	};
	
	/**
	 * CSV headers required from Wowcher.
	 */
	public static final String[] CSV_HEADERS_WOWCHER = {
            "Deal ID",
            "Redeemed at",
            "Wowcher Code",
            "Deal Title",
            "Customer Name",
            "House Number",
            "Address line 1",
            "Address line 2",
            "City",
            "County",
            "Postcode",
            "Email",
            "Phone",
            "Product Name",
            "SKU",
	};
	
	/**
	 * Regular expression used to split CSV lines into separate values.
	 * Looks for commas provided they aren't nested between quotation marks.
	 */
	public static final String SPLITTING_REGEX = ",(?=(?:[^\"]*\"[^\"]*\")*[^\"]*$)";
	
	/**
	 * Compares input headers with expected headers.
	 * @param inputHeaders Headers from the input file.
	 * @param expectedHeaders Headers that we are expecting to see.
	 * @return Whether all expected headers were found; true or false.
	 */
	public static boolean compareHeaders(String[] inputHeaders, String[] expectedHeaders) {
		int matches = 0;
		for (int i = 0; i < expectedHeaders.length; i++) {
			for (int j = 0; j < inputHeaders.length; j++) {
				if (inputHeaders[j].replaceAll("\"", "").equals(expectedHeaders[i])) {
					matches++;
				}
			}
		}
		return (matches == expectedHeaders.length);
	}
	
}