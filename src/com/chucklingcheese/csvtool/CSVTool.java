package com.chucklingcheese.csvtool;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.Stack;

import com.chucklingcheese.csvtool.lang.CSVInfo;
import com.chucklingcheese.csvtool.util.CSVConverter;
import com.chucklingcheese.csvtool.util.CSVCleaner;

/**
 * 
 * The CSV manipulation tool automates the process of examining order export files from online web stores so that we
 * don't have to do it manually.
 * 
 * It works by examining the headers of CSV files it finds in the input folder, and then cleaning, converting, etc. as
 * required based on where the origin of the file is believed to be, before dumping the changes to the output folder.
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 1.0.0
 * 
 */
public class CSVTool {
	
	/**
	 * Counter for errors occurred.
	 */
	private static int countErrors = 0;
	
	/**
	 * Counter for jobs completed.
	 */
	private static int countJobs = 0;
	
	/**
	 * Counter for warnings issued.
	 */
	private static int countWarnings = 0;
	
	/**
	 * Increments the counter for errors occurred.
	 * @return Counter for errors occurred.
	 */
	public static int incErrorCount() {
		return ++countErrors;
	}
	
	/**
	 * Increments the counter for jobs completed.
	 * @return Counter for jobs completed.
	 */
	public static int incJobCount() {
		return ++countJobs;
	}
	
	/**
	 * Increments the counter for warnings issued.
	 * @return Counter for warnings issued.
	 */
	public static int incWarningCount() {
		return ++countWarnings;
	}
	
	/**
	 * Launches the CSV manipulation tool.
	 * @param launchOptions Input received upon launch.
	 */
	public static void main(String[] launchOptions) {
		// Input and output directories.
		File inDirectory = new File("Input");
		File outDirectory = new File("Output");
		
		// Greeter and directory information.
		System.out.println("CSVTool 2.3.1\n-------------");
		System.out.println("Input directory: " + inDirectory.getAbsolutePath());
		System.out.println("Output directory: " + outDirectory.getAbsolutePath());
		
		// Input directory creation and information.
		if (!inDirectory.exists()) {
			if (inDirectory.mkdir()) {
				System.out.println("\nAn empty input directory has been created for you.");
				System.out.println("Please place your input files there and try again.");
			} else {
				System.err.println("\nUnable to create the input directory.");
				System.err.println("Please create this manually, place your input files there, and try again.");
			}
			return;
		}
		
		// Output directory creation and information.
		if (!outDirectory.exists()) {
			if (!outDirectory.mkdir()) {
				System.err.println("\nUnable to create the output directory.");
				System.err.println("Please create this manually and then try again.");
				return;
			}
		}
		
		// Clear the output directory of any pre-existing files.
		for (File outFile : outDirectory.listFiles()) {
			if (!outFile.delete()) {
				System.err.println("\nUnable to clear contents of the output directory.");
				System.err.println("Please remove these files manually and then try again.");
				return;
			}
		}
		
		// Make a note of the start time.
		long startTime = System.currentTimeMillis();
		
		// Examine each file in the input directory.
		for (File inFile : inDirectory.listFiles()) {
			//  For obvious reason we can only examine CSV files.
			if (!inFile.getName().toUpperCase().endsWith(".CSV")) {
				continue;
			}
			System.out.println("\nExamining: " + inFile.getName());
			
			// Process the input file...
			try {
				// Reader and writer used to process the file.
				BufferedReader reader = new BufferedReader(new FileReader(inFile), 0x4000);
				
				// Read the header information from the input file.
				// eBay's CSV files will require us to ignore some lines.
				String lineRead = reader.readLine();
				while(lineRead != null && lineRead.isBlank()) {
					lineRead = reader.readLine();
				}
				if (lineRead == null) {
					System.out.println("Blank input file!");
					reader.close();
					continue;
				}
				String[] headers = lineRead.split(CSVInfo.SPLITTING_REGEX);
				
				// Read each of the following lines and store them in a stack.
				Stack<String> linesRead = new Stack<String>();
				while((lineRead = reader.readLine()) != null) {
					if (!lineRead.isBlank()) {
						linesRead.push(lineRead);
					}
				}
				reader.close();
				
				// TheFoodMarket orders export.
				// Requires cleaning for the OrderWise import.
				if (CSVInfo.compareHeaders(headers, CSVInfo.CSV_HEADERS_TFM)) {
					int count = CSVInfo.incCountTheFoodMarket();
					String fileName = ("TheFoodMarket" + (count > 1 ? (" (" + count + ")") : "") + ".csv");
					System.out.println("TheFoodMarket order exports detected.");
					CSVCleaner.process(headers, linesRead, new File(outDirectory, fileName));
				}
				
				// Everything else requires converting first.
				else {
					CSVConverter.process(headers, linesRead, outDirectory);
				}
			} catch (Exception exception) {
				System.out.println("\nUnreadable input file!");
				exception.printStackTrace(System.out);
			}
		}
		
		// Information dump on jobs, warnings, and errors count.
		long ellapsedTime = System.currentTimeMillis() - startTime;
		String message = "\nCompleted %s jobs in %sms with %s warning(s) and %s error(s).";
		// Top separator.
		for (int i = 0; i < message.length(); i++) {
			System.out.print((i == 0 ? "\n" : "") + "-");
		}
		// Message.
		System.out.println(String.format(message, countJobs, ellapsedTime, countWarnings, countErrors));
		// Bottom separator.
		for (int i = 0; i < message.length(); i++) {
			System.out.print("-" + ((i + 1) == message.length() ? "\n" : ""));
		}
	}
	
}