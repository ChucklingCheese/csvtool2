package com.chucklingcheese.csvtool.util;

import java.io.File;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;

import com.chucklingcheese.csvtool.CSVTool;
import com.chucklingcheese.csvtool.lang.CSVInfo;

/**
 * 
 * Handles the conversion of CSV files containing order information, the OrderWise import is set up to expect the
 * contents to be structured like the files TheFoodMarket produces.
 * 
 * In order to import from other stores we need to first convert them, and then run them through the cleaner.
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 2.1.0
 */
public class CSVConverter {
	
	/**
	 * Convert the contents of order files.
	 * @param headers Headers from the CSV file.
	 * @param linesRead Lines read from the CSV file.
	 * @param outDirectory Directory to store the output file.
	 * @throws Exception In case something goes horribly wrong.
	 */
	public static void process(String[] headers, Stack<String> linesRead, File outDirectory) throws Exception {
		/*
		 * Headers which are referenced should be included in the respective CSVInfo arrays.
		 * This will prevent null-pointer errors occurring when we try to get token values.
		 */
		
		// Some information will need be be obtained immediately.
		// Without knowing this in advance the converter cannot function.
		String storeName = "";
		String storeIdHeader = "";
		String priceHeader = "";
		String deliveryHeader = "";
		String quantityHeader = "Quantity";
		int lineSkips = 0;
		
		// Header checks to determine the origin of the CSV file.
		if (CSVInfo.compareHeaders(headers, CSVInfo.CSV_HEADERS_UDS)) {
			storeName = "uDropship";
			storeIdHeader = "order_id";
			priceHeader = "price";
			deliveryHeader = "shipping_amount";
			quantityHeader = "quantity";
		} else if (CSVInfo.compareHeaders(headers, CSVInfo.CSV_HEADERS_EBAY)) {
			storeName = "eBay";
			storeIdHeader = "Order number";
			priceHeader = "Sold for";
			deliveryHeader = "Postage and packaging";
			lineSkips = 2;
		} else if (CSVInfo.compareHeaders(headers, CSVInfo.CSV_HEADERS_ETSY)) {
			storeName = "Etsy";
			storeIdHeader = "Order ID";
			priceHeader = "Price";
			deliveryHeader = "Order Delivery";
		} else if (CSVInfo.compareHeaders(headers, CSVInfo.CSV_HEADERS_OB)) {
			storeName = "OnBuy";
			storeIdHeader = "Order Number";
			priceHeader = "Product Unit Price";
			deliveryHeader = "Total Delivery";
		} else if (CSVInfo.compareHeaders(headers, CSVInfo.CSV_HEADERS_BB)) {
			storeName = "BoroughBox";
			storeIdHeader = "Order Number";
			priceHeader = "Price";
			deliveryHeader = "Shipping Amount";
			quantityHeader = "Qty";
			lineSkips = 1;
		} else if (CSVInfo.compareHeaders(headers, CSVInfo.CSV_HEADERS_WOWCHER)) {
			storeName = "Wowcher";
			storeIdHeader = "Wowcher Code";
		} else {
			System.out.println("Unrecognised CSV file.");
			return;
		}
		System.out.println(storeName + " order exports detected.");
		
		// uDropship platforms include several headers that are of no practical use to us.
		// These will be offset from the header count prevent them causing synchronisation errors.
		int headerOffsetCount = 0;
		
		// Map each of the headers to its index number in the array.
		// We will use this later to quickly obtain indices via header names.
		HashMap<String, Integer> headerMap = new HashMap<String, Integer>();
		for (int i = 0; i < headers.length; i++) {
			// Ignore uDropship's custom options.
			if (storeName.equals("uDropship") && headers[i].startsWith("custom_option_")) {
				headerOffsetCount++;
				continue;
			}
			// Quotation marks will be removed if present.
			headerMap.put(headers[i].replaceAll("\"", ""), i);
		}
		
		// Value map matches order IDs to the cumulative value of their order.
		// Line parts count map matches order IDs to how many lines their order consists of.
		HashMap<String, Double> valueMap = new HashMap<String, Double>();
		HashMap<String, Integer> linePartsCountMap = new HashMap<String, Integer>();
		
		// Error messages will be accumulated and printed at the end.
		ArrayList<String> errorMessages = new ArrayList<String>();
		
		// As the order lines are reconstructed they will be stored here.
		Stack<String> linesBuilt = new Stack<String>();
		
		// Kill switch to disable writing in case of catastrophic errors.
		boolean disableWriting = false;
		
		// The first (originally last) few lines in some order exports contain no relevant information.
		// Unless these are skipped they will cause issues later when suddenly encountering empty tokens. 
		for (int i = 0; i < lineSkips; i++) {
			if (linesRead.size() >= lineSkips) {
				linesRead.pop();
			}
		}
		
		/*
		 * First pass through the order lines deals with merging and error reporting.
		 * This also greatly simplifies the caching process which is performed further down.
		 */
		main: while(!linesRead.isEmpty()) {
			// The current order line and its tokens.
			String orderLine = linesRead.pop();
			String[] orderTokens = orderLine.split(CSVInfo.SPLITTING_REGEX);
			
			// Ignore any lines that contain no token values.
			if (orderLine.replaceAll(",\"\"", "").length() <= 1) {
				continue;
			}
			
			// Line number which is currently being processed.
			int lineNumber = (linesRead.size() + 2);
			
			// Some order lines may be split up by new line characters present in gift messages.
			// Keep reading additional tokens until we have a complete set matching the length of the headers.
			// As we read from the stack last-in-first-out we can neatly read the trailing message parts first.
			while(orderTokens.length < (headers.length - headerOffsetCount)) {
				// Custom options suddenly having information would cause this.
				// In reality this should otherwise never happen, but you never know...
				if (linesRead.isEmpty()) {
					CSVTool.incErrorCount();
					errorMessages.add("Line number " + lineNumber + " remains incomplete at end-of-file!");
					disableWriting = true;
					break main;
				}
				orderLine = (linesRead.pop() + " " + orderLine);
				orderTokens = orderLine.split(CSVInfo.SPLITTING_REGEX);
			}
			
			// Quotation marks should be present to prevent tokens being split up.
			// If for some reason they aren't then tokens will become "out of sync" with the headers.
			if (orderTokens.length > (headers.length - headerOffsetCount)) {
				CSVTool.incErrorCount();
				errorMessages.add("Line number " + lineNumber + " throws order tokens out of sync!");
				disableWriting = true;
				break main;
			}
			
			// Reconstruct the line from the order tokens and stack them.
			// Padded white space should also be stripped from the tokens here.
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < orderTokens.length; i++) {
				builder.append(orderTokens[i].strip());
				if ((i + 1) != orderTokens.length) {
					builder.append(',');
				}
			}
			linesBuilt.add(builder.toString());
		}
		
		// Set the lines read to the lines built.
		// Conveniently flips our converted output the correct way around.
		linesRead = linesBuilt;
		linesBuilt = new Stack<String>();

		/*
		 * Caching certain values is a necessary evil which may not appear useful at first glance.
		 * Present in TheFoodMarket's CSV files are two troublesome tokens, "Total Value" and "Order Total".
		 * 
		 * "Total Value" is the value of the line (not the whole order) plus its "share" of the shipping.
		 * "Order Total" is the accumulative value of the entire order, i.e. all its lines plus shipping.
		 * 
		 * To generate the correct value for these tokens we must know how many lines each order has been split across.
		 * 
		 * From here we can determine how much of a "share" to give to each of its lines' "Total Value" tokens.
		 * We must also know in advance what the order total is so we can set the "Order Total" tokens correctly.
		 * 
		 * That these two tokens are named so similarly is also incredible frustrating and confusing.
		 * Believe me - things won't work properly for the import unless we do this first!
		 */
		caching: {
			// Wowcher has fixed prices.
			if (storeName.equals("Wowcher")) {
				break caching;
			}
			
			// Add on the product price from other lines pertaining to the same order.
			double price = 0d;
			double delivery = 0d;
			double quantity = 0d;
			int linePartsCount = 0;
			for (int i = 0; i < linesRead.size(); i++) {
				String line = linesRead.elementAt(i);
				String[] lineTokens = line.split(CSVInfo.SPLITTING_REGEX);
				
				// Remove quotation marks as they will break number parsing. 
				for (int j = 0; j < lineTokens.length; j++) {
					lineTokens[j] = lineTokens[j].replaceAll("\"", "");
					// Remove all non-numeric characters (except dots) from monetary values.
					// This likely won't be required for the majority of files being converted.
					if (j == headerMap.get(priceHeader) || j == headerMap.get(deliveryHeader)) {
						lineTokens[j] = lineTokens[j].replaceAll("[^0-9.]", "");
					}
				}
				
				// Acquire the line's order id and retrieve its price and line count if available.
				String lineOrderId = lineTokens[headerMap.get(storeIdHeader)];
				linePartsCount = linePartsCountMap.getOrDefault(lineOrderId, 0);
				price = valueMap.getOrDefault(lineOrderId, 0d);
				
				// Add the shipping fee only once if no value has yet been determined.
				// We'll add up the order's total value as we continue to find its lines.
				delivery = Double.parseDouble(lineTokens[headerMap.get(deliveryHeader)]);
				if (price == 0d && delivery != 0d) {
					price += delivery;
				}
				quantity = Double.parseDouble(lineTokens[headerMap.get(quantityHeader)]);
				price += (Double.parseDouble(lineTokens[headerMap.get(priceHeader)]) * quantity);
				
				// Cache the line's total value and part count thus far.
				valueMap.put(lineOrderId, price);
				linePartsCountMap.put(lineOrderId, ++linePartsCount);
			}
		}
		
		/*
		 * Second pass through the order lines deals with the actual conversion.
		 */
		while(!linesRead.isEmpty() && !disableWriting) {
			String orderLine = linesRead.pop();
			String[] orderTokens = orderLine.split(CSVInfo.SPLITTING_REGEX);
			
			// Used to format monetary values to two decimal places.
			DecimalFormat decimalFormat = new DecimalFormat("##.##");
			
			// Stores are inconsistent with their usage of quotation marks.
			// We'll remove them and re-add them later to make things simpler. 
			for (int i = 0; i < orderTokens.length; i++) {
				orderTokens[i] = orderTokens[i].replaceAll("\"", "");
			}
			
			// Reference used to identify the order.
			String orderId = orderTokens[headerMap.get(storeIdHeader)];
			
			// Some CSV files require some intervention for conversion to work.
			/// During this pass we need to modify the presentation of a few select tokens.
			if (storeName.equals("eBay") || storeName.equals("BoroughBox")) {
				// Remove all non-numeric characters (except dots) from monetary values.
				int[] indices = {
						headerMap.get(priceHeader),
						headerMap.get(deliveryHeader)
				};
				for (int i = 0; i < indices.length; i++) {
					orderTokens[indices[i]] = orderTokens[indices[i]].replaceAll("[^0-9.]", "");
				}
				// Format post-by and sale dates correctly.
				Date date = null;
				Calendar calendar = Calendar.getInstance();
				if (storeName.equals("eBay")) {
					indices = new int[] {
							headerMap.get("Post by date"),
							headerMap.get("Sale date")
					};
				} else {
					indices = new int[] {
							headerMap.get("Order Date")
					};
				}
				// Why eBay (and BoroughBox)? Why?
				for (int i = 0; i < indices.length; i++) {
					if (storeName.equals("eBay")) {
						date = new SimpleDateFormat("dd-MMM-yy").parse(orderTokens[indices[i]]);
					} else {
						date = new SimpleDateFormat("MMM dd, yyyy").parse(orderTokens[indices[i]]);
					}
					calendar.setTime(date);
					int day = calendar.get(Calendar.DAY_OF_MONTH);
					int month = calendar.get(Calendar.MONTH);
					int year = calendar.get(Calendar.YEAR);
					String dayStr = (day < 10 ? "0" + day : Integer.toString(day));
					String monthStr = (++month < 10 ? "0" + month : Integer.toString(month));
					orderTokens[indices[i]] = String.format("%s-%s-%s 00:00:00", year, monthStr, dayStr);
				}
			}
			
			// Reconstruct the line from the order tokens in the order required and stack them.
			// If a header isn't checked for here then it is safe for us to leave it blank.
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < CSVInfo.CSV_HEADERS_TFM.length; i++) {
				// Holds the token we will generate.
				String newToken = "";

				// Order date: YYYY-MM-DD HH-MM-SS +HH:MM
				if (CSVInfo.CSV_HEADERS_TFM[i].equals("Order date")) {
					if (storeName.equals("uDropship")) {
						newToken = (orderTokens[headerMap.get("created_at")]);
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Sale date")];
					} else if (storeName.equals("Etsy")) {
						// Only the date in American format is available.
						String date = orderTokens[headerMap.get("Sale Date")];
						String year = date.substring(6, 8), month = date.substring(0, 2), day = date.substring(3, 5);
						newToken = String.format("20%s-%s-%s 00:00:00", year, month, day);
					} else if (storeName.equals("OnBuy") || storeName.equals("BoroughBox")) {
						newToken = (orderTokens[headerMap.get("Order Date")]);
					} else if (storeName.equals("Wowcher")) {
						newToken = (orderTokens[headerMap.get("Redeemed at")]);
					}
				}
				
				// Order total. Total price of products plus shipping.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Order Total")) {
					// £19 + £2.95 shipping.
					if (storeName.equals("Wowcher")) {
						newToken = "21.95";
					} else {
						newToken = decimalFormat.format(valueMap.get(orderId));
					}
				}
				
				// Order promotions. Always zero.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Order Promotions")) {
					newToken = "0";
				}
				
				// Order ID. Identifies the order.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Store Order Id")) {
					newToken = orderId;
				}
				
				// Order item ID. Unique store-specific product code where available.
				// Think along the lines of ASIN values assigned to products by Amazon.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Store Order Item Id")) {
					if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Item number")];
					} else if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Listing ID")];
					} else if (storeName.equals("OnBuy")) {
						newToken = orderTokens[headerMap.get("OPC")];
					} else if (storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("Deal ID")];
					}
				}
				
				// SKU code. Identifies the product.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("SKU")) {
					if (storeName.equals("uDropship")) {
						// Same as ours but prefixed by our seller ID.
						int index = orderTokens[headerMap.get("sku")].indexOf('-') + 1;
						newToken = orderTokens[headerMap.get("sku")].substring(index);
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Custom label")];
					} else if (storeName.equals("BoroughBox")) {
						// Same as ours but prefixed by our seller ID.
						// This only needs to be removed for uDropship-era products.
						int index = orderTokens[headerMap.get("SKU")].indexOf('-') + 1;
						newToken = orderTokens[headerMap.get("SKU")].substring(index);
					} else {
						newToken = orderTokens[headerMap.get("SKU")];
					}
				}
				
				// Item name. Identifies the product, but not as helpful.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Item Name")) {
					if (storeName.equals("uDropship")) {
						newToken = orderTokens[headerMap.get("product")];
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Item title")];
					} else if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Item Name")];
					} else if (storeName.equals("OnBuy") || storeName.equals("BoroughBox") || storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("Product Name")];
					}
				}
				
				// Quantity.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Quantity")) {
					if (storeName.equals("uDropship")) {
						// All decimal places need to be removed.
						String quantity = orderTokens[headerMap.get(quantityHeader)];
						newToken = quantity.substring(0, quantity.indexOf('.'));
					} else if (storeName.equals("Wowcher")) {
						newToken = "1";
					} else {
						newToken = orderTokens[headerMap.get(quantityHeader)];
					}
				}
				
				// Price of the product.
				// The "Product Value" token after this is unused.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Amount")) {
					if (storeName.equals("uDropship")) {
						// Some decimal places need to be removed.
						double price = Double.parseDouble(orderTokens[headerMap.get(priceHeader)]);
						newToken = decimalFormat.format(price);
					} else if (storeName.equals("Wowcher")) {
						newToken = "19.00";
					} else {
						newToken = orderTokens[headerMap.get(priceHeader)];
					}
				}
				
				// Shipping cost "share" for this order line.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Delivery Cost")) {
					if (storeName.equals("Wowcher")) {
						newToken = "2.95";
					} else {
						double delivery = Double.parseDouble(orderTokens[headerMap.get(deliveryHeader)]);
						newToken = decimalFormat.format((delivery / linePartsCountMap.get(orderId)));
					}
				}
				
				// Price of the product plus its share of the shipping cost.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Total Value")) {
					if (storeName.equals("Wowcher")) {
						newToken = "21.95";
					} else {
						double price = Double.parseDouble(orderTokens[headerMap.get(priceHeader)]);
						double delivery = Double.parseDouble(orderTokens[headerMap.get(deliveryHeader)]);
						double quantity = Double.parseDouble(orderTokens[headerMap.get(quantityHeader)]);
						newToken = decimalFormat.format((price * quantity) + (delivery / linePartsCountMap.get(orderId)));
					}
				}
				
				// Currency code.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Currency Code")) {
					if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Currency")];
					} else {
						newToken = "GBP";
					}
				}
				
				// Delivery name.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Delivery Name")) {
					if (storeName.equals("uDropship")) {
						// Split up across multiple tokens.
						String compName = orderTokens[headerMap.get("ship_company")];
						String firstName = orderTokens[headerMap.get("ship_firstname")];
						String lastName = orderTokens[headerMap.get("ship_lastname")];
						String fullName = (firstName + " " + lastName);
						newToken = ((compName.isBlank() ? "" : (compName + ", ")) + fullName);
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Post to name")];
					} else if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Delivery Name")];
					} else if (storeName.equals("OnBuy")) {
						newToken = orderTokens[headerMap.get("Delivery Address Name")];
					} else if (storeName.equals("BoroughBox")) {
						String firstName = orderTokens[headerMap.get("Firstname")];
						String lastName = orderTokens[headerMap.get("Lastname")];
						newToken = (firstName + " " + lastName);
					} else if (storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("Customer Name")];
					}
				}
				
				// Delivery address; first line.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Delivery Address 1")) {
					if (storeName.equals("uDropship")) {
						newToken = orderTokens[headerMap.get("ship_address_1")];
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Post to address 1")];
					} else if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Delivery Address1")];
					} else if (storeName.equals("OnBuy")) {
						newToken = orderTokens[headerMap.get("Delivery Address Line 1")];
					} else if (storeName.equals("BoroughBox")) {
						newToken = orderTokens[headerMap.get("Address 1")];
					} else if (storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("Address line 1")];
					}
				}
				
				// Delivery address; second line where available.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Delivery Address 2")) {
					if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Post to address 2")];
					} else if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Delivery Address2")];
					} else if (storeName.equals("OnBuy")) {
						newToken = orderTokens[headerMap.get("Delivery Address Line 2")];
					} else if (storeName.equals("BoroughBox")) {
						newToken = orderTokens[headerMap.get("Address 2")];
					} else if (storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("Address line 2")];
					}
				}
				
				// Delivery address; third line where available.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Delivery Address 3")) {
					if (storeName.equals("OnBuy")) {
						newToken = orderTokens[headerMap.get("Delivery Address Line 3")];
					}
				}
				
				// Delivery town or city.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Delivery Town City")) {
					if (storeName.equals("uDropship")) {
						newToken = orderTokens[headerMap.get("ship_city")];
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Post to city")];
					} else if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Delivery City")];
					} else if (storeName.equals("OnBuy")) {
						newToken = orderTokens[headerMap.get("Delivery Address Town")];
					} else if (storeName.equals("BoroughBox") || storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("City")];
					}
				}
				
				// Delivery county.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Delivery State County")) {
					if (storeName.equals("uDropship")) {
						newToken = orderTokens[headerMap.get("ship_region")];
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Post to county")];
					} else if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Delivery State")];
					} else if (storeName.equals("OnBuy")) {
						newToken = orderTokens[headerMap.get("Delivery Address County")];
					} else if (storeName.equals("BoroughBox")) {
						newToken = orderTokens[headerMap.get("State")];
					} else if (storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("County")];
					}
				}
				
				// Delivery post code
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Post code/Zip")) {
					if (storeName.equals("uDropship")) {
						newToken = orderTokens[headerMap.get("ship_postcode")];
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Post to postcode")];
					} else if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Delivery Zipcode")];
					} else if (storeName.equals("OnBuy")) {
						newToken = orderTokens[headerMap.get("Delivery Address Postcode")];
					} else if (storeName.equals("BoroughBox")) {
						newToken = orderTokens[headerMap.get("Zip Code")];
					} else if (storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("Postcode")];
					}
				}
				
				// Delivery country.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Delivery Country")) {
					if (storeName.equals("uDropship")) {
						// Uses country codes rather than names.
						if (orderTokens[headerMap.get("ship_country")].equals("GB")) {
							newToken = "United Kingdom";
						} else {
							newToken = "Foreign Country";
						}
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Post to country")];
					} else if (storeName.equals("Etsy")) {
						newToken = orderTokens[headerMap.get("Delivery Country")];
					} else if (storeName.equals("OnBuy")) {
						newToken = orderTokens[headerMap.get("Delivery Address Country")];
					} else if (storeName.equals("BoroughBox")) {
						newToken = orderTokens[headerMap.get("Country")];
					} else if (storeName.equals("Wowcher")) {
						newToken = "United Kingdom";
					}
				}
				
				// Customer phone number where available.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Customer Phone Number")) {
					if (storeName.equals("uDropship")) {
						newToken =  orderTokens[headerMap.get("ship_telephone")];
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Post to phone")];
					} else if (storeName.equals("OnBuy")) {
						// Prefixed by the customer name.
						String customer = orderTokens[headerMap.get("Customer")];
						int index = customer.lastIndexOf(',');
						if (index > -1) {
							newToken = customer.substring(index + 2);
						}
					} else if (storeName.equals("BoroughBox") || storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("Phone")];
					}
				}
				
				// Shipping status. Always "ready".
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Shipping Status")) {
					newToken = "ready";
				}
				
				// Shipping type. Always "Courier". 
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Shipping Type")) {
					newToken = "Courier";
				}
				
				// Delivery due date.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Delivery Due Date")) {
					if (storeName.equals("uDropship")) {
						newToken = orderTokens[headerMap.get("created_at")].substring(0, 10);
					} else if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Post by date")];
					} else if (storeName.equals("Etsy")) {
						// Only the date in American format is available.
						String date = orderTokens[headerMap.get("Sale Date")];
						String year = date.substring(6, 8), month = date.substring(0, 2), day = date.substring(3, 5);
						newToken = String.format("20%s-%s-%s", year, month, day);
					} else if (storeName.equals("OnBuy") || storeName.equals("BoroughBox")) {
						newToken = orderTokens[headerMap.get("Order Date")].substring(0, 10);
					} else if (storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("Redeemed at")];
					}
				}
				
				// Dispatch status; always false.
				// Everything between this and the gift message is blank.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Dispatched")) {
					newToken = "false";
				}
				
				// Gift message to the recipient where available.
				// Generic "From John Smith"-style messages are used if absent.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Gift Message")) {
					if (storeName.equals("uDropship")) {
						// Split up across multiple tokens.
						newToken = orderTokens[headerMap.get("message")];
						if (newToken.isBlank()) {
							String compName = orderTokens[headerMap.get("bill_company")];
							String firstName = orderTokens[headerMap.get("bill_firstname")];
							String lastName = orderTokens[headerMap.get("bill_lastname")];
							String fullName = (firstName + " " + lastName);
							newToken = ("From " + (compName.isBlank() ? "" : (compName + ", ")) + fullName);
						}
					} else if (storeName.equals("eBay")) {
						newToken = ("From " + orderTokens[headerMap.get("Buyer name")]);
					} else if (storeName.equals("Etsy")) {
						newToken = ("From " + orderTokens[headerMap.get("Buyer")]);
					} else if (storeName.equals("OnBuy")) {
						// Suffixed by the phone number.
						String customer = orderTokens[headerMap.get("Customer")];
						int index = customer.lastIndexOf(',');
						if (index > -1) {
							newToken = ("From " + customer.substring(0, index));
						}
					} else if (storeName.equals("BoroughBox")) {
						String firstName = orderTokens[headerMap.get("Firstname")];
						String lastName = orderTokens[headerMap.get("Lastname")];
						newToken = ("From " + firstName + " " + lastName);
					} else if (storeName.equals("Wowcher")) {
						newToken = ("From " + orderTokens[headerMap.get("Customer Name")]);
					}
				}
				
				// Shipment state. Always "ready".
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Shipment State")) {
					newToken = "ready";
				}
				
				// Email address.
				else if (CSVInfo.CSV_HEADERS_TFM[i].equals("Email")) {
					if (storeName.equals("eBay")) {
						newToken = orderTokens[headerMap.get("Buyer email")];
					} else if (storeName.equals("Wowcher")) {
						newToken = orderTokens[headerMap.get("Email")];
					}
				}
				
				// Surround order tokens with quotes and add a comma if necessary.
				// This is so we can split up the tokens again in the cleanup later.
				builder.append("\"" + newToken + "\"");
				if ((i + 1) != CSVInfo.CSV_HEADERS_TFM.length) {
					builder.append(',');
				}
			}
			
			// Add the line to the stack and process the next...
			linesBuilt.push(builder.toString());
		}
		
		// Print the accumulated error messages.
		if (!errorMessages.isEmpty() ) {
			System.out.println("\n--- ERRORS ---");
			Iterator<String> iterator = errorMessages.iterator();
			while (iterator.hasNext()) {
				System.out.println(iterator.next());
			}
			System.out.println("--------------");
		}

		// Determine the output file name and use the cleaner.
		if (!disableWriting) {
			int count = 0;
			if (storeName.equals("uDropship")) {
				count = CSVInfo.incCountuDropship();
			} else if (storeName.equals("eBay")) {
				count = CSVInfo.incCountEbay();
			} else if (storeName.equals("Etsy")) {
				count = CSVInfo.incCountEtsy();
			} else if (storeName.equals("OnBuy")) {
				count = CSVInfo.incCountOnBuy();
			} else if (storeName.equals("BoroughBox")) {
				count = CSVInfo.incCountBoroughBox();
			} else if (storeName.equals("Wowcher")) {
				count = CSVInfo.incCountWowcher();
			}
			String fileName = (storeName + (count > 1 ? (" (" + count + ")") : "") + ".csv");
			CSVCleaner.process(CSVInfo.CSV_HEADERS_TFM, linesBuilt, new File(outDirectory, fileName));
		}
	}
}