package com.chucklingcheese.csvtool.util;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Stack;

import com.chucklingcheese.csvtool.CSVTool;
import com.chucklingcheese.csvtool.lang.CSVInfo;
import com.chucklingcheese.csvtool.lang.PostageInfo;

/**
 * 
 * Handles the cleaning-up of CSV files containing order information, the only caveat is that the contents needs to be
 * structured in the way we receive them from TheFoodMarket.
 * 
 * All CSV files from other stores, such as BoroughBox, OnBuy, and Esty, will need to first be be run through the
 * conversion tool, which will reorganise the contents and make them compatible. 
 * 
 * @author Darryl (darryl@chucklingcheese.co.uk)
 * 
 * @since 2.0.0
 * 
 */
public class CSVCleaner {
	
	/**
	 * Clean up the contents order files.
	 * @param headers Headers from the CSV file.
	 * @param linesRead Lines read from the CSV file.
	 * @throws Exception In case something goes horribly wrong.
	 */
	public static void process(String[] headers, Stack<String> linesRead, File outFile) throws Exception {
		// Map each of the headers to its index number in the array.
		// We will use this later to quickly obtain indices via header names.
		HashMap<String, Integer> headerMap = new HashMap<String, Integer>();
		for (int i = 0; i < headers.length; i++) {
			headerMap.put(headers[i], i);
		}
		
		// IDs with errors prevent any lines for an order from being written.
		// IDs with warnings suppress repeat warnings for its continuing lines.
		// Error and warning messages will be accumulated and printed at the end.
		ArrayList<String> errorOrderIds = new ArrayList<String>();
		ArrayList<String> warningOrderIds = new ArrayList<String>();
		ArrayList<String> errorMessages = new ArrayList<String>();
		ArrayList<String> warningMessages = new ArrayList<String>();
		
		// As the order lines are reconstructed they will be stored here.
		Stack<String> linesBuilt = new Stack<String>();
		
		// Kill switch to disable writing in case of catastrophic errors.
		boolean disableWriting = false;
		
		// Process each of the lines read from the input file.
		main: while(!linesRead.isEmpty()) {
			String orderLine = linesRead.pop();
			String[] orderTokens = orderLine.split(CSVInfo.SPLITTING_REGEX);
			
			// Line number which is currently being processed.
			int lineNumber = (linesRead.size() + 2);
			
			// Some order lines may be split up by new line characters present in gift messages.
			// Keep reading additional tokens until we have a complete set matching the length of the headers.
			// As we read from the stack last-in-first-out we can neatly read the trailing message parts first.
			while(orderTokens.length < headers.length) {
				// In reality this should never happen, but you never know...
				if (linesRead.isEmpty()) {
					CSVTool.incErrorCount();
					errorMessages.add("Line number " + lineNumber + " remains incomplete at end-of-file!");
					disableWriting = true;
					break main;
				}
				orderLine = (linesRead.pop() + " " + orderLine);
				orderTokens = orderLine.split(CSVInfo.SPLITTING_REGEX);
			}
			
			// Quotation marks should be present to prevent tokens being split up.
			// If for some reason they aren't then tokens will become "out of sync" with the headers.
			// We can thank TheFoodMarket and order CR056465755 for making this check a necessity.
			if (orderTokens.length > headers.length) {
				CSVTool.incErrorCount();
				errorMessages.add("Line number " + lineNumber + " throws order tokens out of sync!");
				disableWriting = true;
				break main;
			}
			
			// Remove quotation marks and and re-add them later to make things simpler.
			// Padded white space should also be stripped from the tokens here.
			for (int i = 0; i < orderTokens.length; i++) {
				orderTokens[i] = orderTokens[i].replaceAll("\"", "").strip();
			}
			
			// Reference used to identify the order.
			String orderId = orderTokens[headerMap.get("Store Order Id")];
			
			// Skip this line if a fatal error previously occurred with this order id.
			if (errorOrderIds.contains(orderId)) {
				continue;
			}
			
			// Suppress warnings if any were reporting with this order id.
			// This only affects information which would be identical anyway.
			boolean suppressErrors = false;
			if (warningOrderIds.contains(orderId)) {
				suppressErrors = true;
			}
			
			/***********************************/
			/***** DELIVERY ADDRESS CHECKS *****/
			/***********************************/
			
			// Indices for delivery address information minus the country and post code.
			int[] deliveryIndices = {
					headerMap.get("Delivery Name"), headerMap.get("Delivery Address 1"),
					headerMap.get("Delivery Address 2"), headerMap.get("Delivery Address 3"),
					headerMap.get("Delivery Town City"), headerMap.get("Delivery State County")
			};
			
			// Each of these order tokens should be converted to "camel case" for easier legibility.
			for (int index : deliveryIndices) {
				// Remove all unnecessary additional white space from the token.
				orderTokens[index] = orderTokens[index].replaceAll("\\s+", " ");
				// Split the token into words and capitalise only each word's first character.
				StringBuilder builder = new StringBuilder();
				String[] words = orderTokens[index].split(" ");
				for (String word : words) {
					if (!word.isBlank()) {
						builder.append(Character.toUpperCase(word.charAt(0)));
						// If there is more to add, convert it to lower case.
						if (word.length() > 0) {
							builder.append(word.substring(1).toLowerCase());
						}
					}
					// Add a space before the next word in this token.
					if (builder.length() != orderTokens[index].length()) {
						builder.append(" ");
					}
				}
				// Replace the original token.
				orderTokens[index] = builder.toString();
			}
			
			// Delivery name must exist.
			int index = headerMap.get("Delivery Name");
			if (orderTokens[index].isBlank()) {
				CSVTool.incErrorCount();
				errorOrderIds.add(orderId);
				errorMessages.add(orderId + " has a missing delivery name!");
				continue;
			}
			
			// Delivery address line one must exist.
			index = headerMap.get("Delivery Address 1");
			if (orderTokens[index].isBlank()) {
				CSVTool.incErrorCount();
				errorOrderIds.add(orderId);
				errorMessages.add(orderId + " has a missing delivery address line!");
				continue;
			}
			
			// Delivery town or city must exist.
			index = headerMap.get("Delivery Town City");
			if (orderTokens[index].isBlank()) {
				CSVTool.incErrorCount();
				errorOrderIds.add(orderId);
				errorMessages.add(orderId + " has a missing delivery town or city!");
				continue;
			}
			
			// We must "camel-case" any references to the United Kingdom for the OrderWise import.
			// If other countries are accepted at a later date this should be dealt with elsewhere.
			index = headerMap.get("Delivery Country");
			if (orderTokens[index].equalsIgnoreCase("United Kingdom")) {
				orderTokens[index] = "United Kingdom";
			} else if (orderTokens[index].isBlank()) {
				// Delivery country must exist.
				CSVTool.incErrorCount();
				errorOrderIds.add(orderId);
				errorMessages.add(orderId + " has a missing delivery country!");
				continue;
			} else {
				// Delivery country cannot be foreign.
				CSVTool.incErrorCount();
				errorOrderIds.add(orderId);
				errorMessages.add(orderId + " has a non-British delivery country: " + orderTokens[index]);
				continue;
			}
			
			/****************************/
			/***** POST CODE CHECKS *****/
			/****************************/
			
			// Post codes need to have multiple checks done to them.
			// If a customer has input incorrect information it is usually this.
			index = headerMap.get("Post code/Zip");
			orderTokens[index] = orderTokens[index].toUpperCase().replaceAll("\\s+", "");
			
			// Post codes must exist.
			if (orderTokens[index].isBlank()) {
				CSVTool.incErrorCount();
				errorOrderIds.add(orderId);
				errorMessages.add(orderId + " has a missing post code.");
				continue;
			} else if (orderTokens[index].length() < 5 || orderTokens[index].length() > 7) {
				// Post codes are always between five to eight characters.
				CSVTool.incErrorCount();
				errorOrderIds.add(orderId);
				errorMessages.add(orderId + " has a post code of incorrect length: " + orderTokens[index]);
				continue;
			} else {
				int split = orderTokens[index].length() - 3;
				String outward = orderTokens[index].substring(0, split);
				String inward = orderTokens[index].substring(split);
				orderTokens[index] = (outward + " " + inward);
			}
			
			// Detect post codes that aren't in current use anymore.
			for (int i = 0; i < PostageInfo.DEPRECATED.length; i++) {
				if (orderTokens[index].startsWith(PostageInfo.DEPRECATED[i])) {
					CSVTool.incErrorCount();
					errorOrderIds.add(orderId);
					errorMessages.add(orderId + " has a deprecated post code: " + orderTokens[index]);
					continue;
				}
			}
			
			// Detect post codes that are for British Overseas Territories.
			for (int i = 0; i < PostageInfo.DEPRECATED.length; i++) {
				if (orderTokens[index].startsWith(PostageInfo.OVERSEAS_TERRITORIES[i])) {
					CSVTool.incErrorCount();
					errorOrderIds.add(orderId);
					errorMessages.add(orderId + " has an overseas territory post code: " + orderTokens[index]);
					continue;
				}
			}
			
			// Detect post codes that are for non-geographic locations.
			for (int i = 0; i < PostageInfo.DEPRECATED.length; i++) {
				if (orderTokens[index].startsWith(PostageInfo.NON_GEOGRAPHIC[i])) {
					CSVTool.incErrorCount();
					errorOrderIds.add(orderId);
					errorMessages.add(orderId + " has a non-geographic post code: " + orderTokens[index]);
					continue;
				}
			}
			
			/************************************/
			/***** EXTRA INFORMATION CHECKS *****/
			/************************************/
			
			/*
			 * GIFT MESSAGES
			 */
			
			// Remove double spaces created when merging gift message lines.
			index = headerMap.get("Gift Message");
			orderTokens[index] = orderTokens[index].replaceAll("\\s+", " ");
			
			/*
			 * SKU CODES
			 */
			index = headerMap.get("SKU");
			if (orderTokens[index].isBlank()) {
				CSVTool.incErrorCount();
				errorOrderIds.add(orderId);
				errorMessages.add(orderId + " has not provided an SKU code!");
				continue;
			}
			
			/*
			 * PHONE NUMBERS
			 */
			
			// Fix up phone numbers to help ensure customers get their tracking codes.
			index = headerMap.get("Customer Phone Number");
			orderTokens[index] = orderTokens[index].replaceAll("\\s+", "");
			
			// Remove all non-numeric characters from the phone number.
			orderTokens[index] = orderTokens[index].replaceAll("[^0-9]", "");
			
			// Check for issues with phone numbers and try to fix them.
			if (!orderTokens[index].isBlank() && orderTokens[index].length() >= 10) {
				// Switch the zero at the start of the phone number to the calling code.
				if (orderTokens[index].charAt(1) >= '1' && orderTokens[index].charAt(1) <= '9') {
					if (orderTokens[index].charAt(1) != '4' && orderTokens[index].charAt(1) != '6') {
						int offset = (orderTokens[index].startsWith("0") ? 1 : 0);
						orderTokens[index] = "44" + orderTokens[index].substring(offset);
					}
				}
				
				// Where neither a zero or calling code exist.
				if (!orderTokens[index].startsWith("0") && orderTokens[index].length() == 10) {
					if (!orderTokens[index].startsWith("4") && !orderTokens[index].startsWith("6")) {
						orderTokens[index] = "+44" + orderTokens[index];
					}
				}
				
				// Where the opposite has happened and we've gotten double of each.
				// Censored real-life example: "00 44 7703 ??????"
				if (orderTokens[index].startsWith("0044")) {
					orderTokens[index] = "+44" + orderTokens[index].substring(4);
				}
				
				// Other calling code-related fixes.
				if (orderTokens[index].startsWith("44")) {
					orderTokens[index] = "+" + orderTokens[index];
					
					// When the calling code and zero are both present.
					// Censored real-life example: "+44 07545 ??????"
					if (orderTokens[index].startsWith("+440")) {
						orderTokens[index] = orderTokens[index].replace("+440", "+44");
					}
					// When the calling code has been added twice.
					// Censored real-life example: "+44 447974??????"
					if (orderTokens[index].startsWith("+4444")) {
						orderTokens[index] = orderTokens[index].replace("+4444", "+44");
					}
				}
			}
			
			// Print a warning if there's still something weird so we can manually look.
			if (!orderTokens[index].isBlank() && orderTokens[index].length() != 13) {
				if (!suppressErrors) {
					CSVTool.incWarningCount();
					warningOrderIds.add(orderId);
					warningMessages.add(orderId + " has a questionable phone number: " + orderTokens[index]);
				}
			}
			
			/*
			 * EMAIL ADDRESSES
			 */
			
			// Fix up email addresses to help ensure customers get their tracking codes.
			index = headerMap.get("Email");
			orderTokens[index] = orderTokens[index].replaceAll("\\s+", "");
			if (!orderTokens[index].isBlank() && !orderTokens[index].contains("@")) {
				CSVTool.incWarningCount();
				warningOrderIds.add(orderId);
				warningMessages.add(orderId + " has a questionable email address: " + orderTokens[index]);
			}
			
			/**************************/
			/***** RECONSTRUCTION *****/
			/**************************/
			
			// Reconstruct the line from the order tokens in the order required and stack them.
			StringBuilder builder = new StringBuilder();
			for (int i = 0; i < CSVInfo.CSV_HEADERS_TFM.length; i++) {
				if (headerMap.containsKey(CSVInfo.CSV_HEADERS_TFM[i])) {
					index = headerMap.get(CSVInfo.CSV_HEADERS_TFM[i]);
					if (orderTokens[index].contains(",") || orderTokens[index].isBlank()) {
						orderTokens[index] = ("\"" + orderTokens[index] + "\"");
					}
					builder.append(orderTokens[index] + ((index + 1) != CSVInfo.CSV_HEADERS_TFM.length ? "," : ""));
				} else {
					CSVTool.incErrorCount();
					errorMessages.add("Line number " + lineNumber + " lacks a token: " + CSVInfo.CSV_HEADERS_TFM[i]);
					disableWriting = true;
					break main;
				}
			}
			
			// Add the line to the stack and process the next...
			linesBuilt.push(builder.toString());
		}
		
		// Reconstruct the header line and add it to the lines built.
		// We have to add this last so that it's the first to be written.
		StringBuilder builder = new StringBuilder();
		for (int i = 0; i < CSVInfo.CSV_HEADERS_TFM.length; i++) {
			builder.append(CSVInfo.CSV_HEADERS_TFM[i] + ((i + 1) != CSVInfo.CSV_HEADERS_TFM.length ? "," : ""));
		}
		linesBuilt.push(builder.toString());
		
		// Write each of the lines in the stack.
		if (!disableWriting) {
			BufferedWriter writer = new BufferedWriter(new FileWriter(outFile), 0x4000);
			// Headers are written separately to not count as a job.
			if (!linesBuilt.isEmpty() && !disableWriting) {
				writer.write(linesBuilt.pop());
				writer.newLine();
			}
			// Dump all of the built lines into the output file.
			while(!linesBuilt.isEmpty() && !disableWriting) {
				writer.write(linesBuilt.pop());
				writer.newLine();
				CSVTool.incJobCount();
			}
			writer.close();
		}
		
		// Print the accumulated warning messages.
		if (!warningMessages.isEmpty() ) {
			System.out.println("\n--- WARNINGS ---");
			Iterator<String> iterator = warningMessages.iterator();
			while (iterator.hasNext()) {
				System.out.println(iterator.next());
			}
			System.out.println("----------------");
		}
		
		// Print the accumulated error messages.
		if (!errorMessages.isEmpty() ) {
			System.out.println("\n--- ERRORS ---");
			Iterator<String> iterator = errorMessages.iterator();
			while (iterator.hasNext()) {
				System.out.println(iterator.next());
			}
			System.out.println("--------------");
		}
	}
	
}